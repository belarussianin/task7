package com.example.task7

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import com.example.task7.common.TAG
import com.example.task7.common.appComponent
import com.example.task7.ui.screen.main.components.Home
import com.example.task7.ui.screen.main.model.MainViewModel
import com.example.task7.ui.theme.Task7Theme

class MainActivity : ComponentActivity() {

    private val viewModel by viewModels<MainViewModel> { appComponent.viewModelsFactory() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val formState = viewModel.form.collectAsState()
            val resultState = viewModel.result.collectAsState()

            Task7Theme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    Home(formState.value, resultState.value) {
                        Log.i(TAG, "mainActivity post: ${it.form.entries.joinToString(", ")}")
                        viewModel.post(it)
                    }
                }
            }
        }
    }
}