package com.example.task7.di.module

import dagger.Module

@Module(includes = [AppBindModule::class, NetworkModule::class, ViewModelModule::class])
class AppModule