package com.example.task7.di.component

import com.example.task7.di.module.AppModule
import com.example.task7.di.module.ViewModelModule
import com.example.task7.MainActivity
import dagger.Component
import javax.inject.Singleton

@Component(modules = [AppModule::class])
@Singleton
interface AppComponent {
    fun viewModelsFactory(): ViewModelModule.ViewModelsFactory
    fun inject(activity: MainActivity)
}