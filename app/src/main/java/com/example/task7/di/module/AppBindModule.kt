package com.example.task7.di.module

import com.example.task7.data.repository.TestRepositoryImpl
import com.example.task7.domain.repository.TestRepository
import dagger.Binds
import dagger.Module

@Module
interface AppBindModule {

    @Suppress("FunctionName")
    @Binds
    fun bindTestRepositoryImpl_to_TestRepository(
        bankRepositoryImpl: TestRepositoryImpl
    ): TestRepository
}