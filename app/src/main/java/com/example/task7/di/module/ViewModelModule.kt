package com.example.task7.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.task7.ui.screen.main.model.MainViewModel
import com.example.task7.di.annotation.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import javax.inject.Inject
import javax.inject.Provider
import javax.inject.Singleton

@Module
abstract class ViewModelModule {

    @Singleton
    class ViewModelsFactory @Inject constructor(
        private val viewModelFactories: Map<Class<out ViewModel>, @JvmSuppressWildcards Provider<ViewModel>>
    ) : ViewModelProvider.Factory {

        @Suppress("Unchecked_cast")
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return viewModelFactories.getValue(modelClass as Class<out ViewModel>).get() as T
        }
    }

    @Binds
    @[IntoMap ViewModelKey(MainViewModel::class)]
    abstract fun mainViewModel(mainViewModel: MainViewModel): ViewModel
}