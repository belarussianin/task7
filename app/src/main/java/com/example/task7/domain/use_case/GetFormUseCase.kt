package com.example.task7.domain.use_case

import com.example.task7.data.remote.dto.GetFormDto
import com.example.task7.domain.repository.TestRepository
import javax.inject.Inject

class GetFormUseCase @Inject constructor(
    private val repository: TestRepository
) : BaseUseCase<GetFormDto, Nothing?>() {
    override suspend fun run(params: Nothing?) = repository.get()
}