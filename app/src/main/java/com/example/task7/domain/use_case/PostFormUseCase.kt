package com.example.task7.domain.use_case

import com.example.task7.data.remote.dto.PostFormDto
import com.example.task7.data.remote.dto.Result
import com.example.task7.domain.repository.TestRepository
import javax.inject.Inject

class PostFormUseCase @Inject constructor(
    private val repository: TestRepository
) : BaseUseCase<Result, PostFormDto>() {
    override suspend fun run(params: PostFormDto) = repository.post(params)
}