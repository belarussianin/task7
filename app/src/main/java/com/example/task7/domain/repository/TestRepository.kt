package com.example.task7.domain.repository

import com.example.task7.data.remote.dto.GetFormDto
import com.example.task7.data.remote.dto.PostFormDto
import com.example.task7.data.remote.dto.Result

interface TestRepository {
    suspend fun get(): GetFormDto
    suspend fun post(postFormDto: PostFormDto): Result
}