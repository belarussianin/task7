package com.example.task7.domain.viewModel

import android.util.Log
import androidx.lifecycle.ViewModel
import com.example.task7.common.TAG
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.retryWhen
import okio.IOException

abstract class BaseViewModel : ViewModel() {

    protected fun <T : Any> handleNetworkFlow(
        flow: Flow<T>,
        retryDelay: Long = 5000,
        onEach: suspend (T) -> Unit,
        catch: suspend FlowCollector<T>.(Throwable) -> Unit
    ): Flow<T> {
        return flow
            .onEach(onEach).catch(catch)
            .retryWhen { cause, attempt ->
                if (attempt == 0L) {
                    Log.e(TAG, "handleNetworkFlow: error")
                }
                delay(retryDelay)
                cause is IOException
            }
    }
}