package com.example.task7.ui.screen.main.components

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.DropdownMenu
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.KeyboardType
import com.example.task7.data.remote.dto.Field
import com.example.task7.data.remote.dto.PostField
import com.example.task7.data.remote.dto.Type

@Composable
fun FormFieldItem(
    field: Field,
    save: Boolean,
    onSave: (PostField) -> Unit
) {
    val textFieldState = rememberSaveable { mutableStateOf("") }
    field.apply {
        Row {
            title?.let {
                Text(text = "$it: ")
            }
            when (type) {
                Type.Text.string -> {
                    OutlinedTextField(
                        value = textFieldState.value,
                        modifier = Modifier.fillMaxWidth(),
                        keyboardOptions = KeyboardOptions.Default.copy(keyboardType = KeyboardType.Text),
                        onValueChange = {
                            textFieldState.value = it
                        }, singleLine = true, maxLines = 1
                    )
                    if (save) {
                        onSave(
                            PostField(
                                name = name ?: "",
                                value = textFieldState.value
                            )
                        )
                    }
                }
                Type.Numeric.string -> {
                    OutlinedTextField(
                        value = textFieldState.value,
                        modifier = Modifier.fillMaxWidth(),
                        keyboardOptions = KeyboardOptions.Default.copy(keyboardType = KeyboardType.Number),
                        onValueChange = {
                            textFieldState.value = it
                        }, singleLine = true, maxLines = 1
                    )
                    if (save) {
                        onSave(
                            PostField(
                                name = name ?: "",
                                value = textFieldState.value.replace(',', '.')
                            )
                        )
                    }
                }
                Type.List.string -> {
                    var expanded by rememberSaveable { mutableStateOf(false) }
                    val items = listOf(
                        values?.none ?: "",
                        values?.v1 ?: "",
                        values?.v2 ?: "",
                        values?.v3 ?: ""
                    )
                    var selectedIndex by rememberSaveable { mutableStateOf(0) }
                    Box(
                        modifier = Modifier
                            .fillMaxSize()
                            .wrapContentSize(Alignment.TopStart)
                    ) {
                        Text(
                            items[selectedIndex],
                            modifier = Modifier
                                .fillMaxWidth()
                                .clickable(onClick = { expanded = true })
                                .background(
                                    androidx.compose.ui.graphics.Color.Gray
                                )
                        )
                        DropdownMenu(
                            expanded = expanded,
                            onDismissRequest = { expanded = false },
                            modifier = Modifier
                                .fillMaxWidth()
                        ) {
                            items.forEachIndexed { index, item ->
                                DropdownMenuItem(onClick = {
                                    selectedIndex = index
                                    expanded = false
                                }) {
                                    Text(text = item)
                                }
                            }
                        }
                    }
                    if (save) {
                        onSave(
                            PostField(
                                name = name ?: "",
                                value = items[selectedIndex]
                            )
                        )
                    }
                }
            }
        }
    }
}