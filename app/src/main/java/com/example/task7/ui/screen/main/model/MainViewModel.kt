package com.example.task7.ui.screen.main.model

import android.util.Log
import androidx.lifecycle.viewModelScope
import com.example.task7.common.TAG
import com.example.task7.common.UiState
import com.example.task7.data.remote.dto.GetFormDto
import com.example.task7.data.remote.dto.PostFormDto
import com.example.task7.domain.use_case.GetFormUseCase
import com.example.task7.domain.use_case.PostFormUseCase
import com.example.task7.domain.viewModel.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val getFormUseCase: GetFormUseCase,
    private val postFormUseCase: PostFormUseCase
) : BaseViewModel() {

    private val _form = MutableStateFlow<UiState<GetFormDto, String>>(UiState.Loading)
    val form get() = _form.asStateFlow()

    private val _result = MutableStateFlow<UiState<String, String>>(UiState.Loading)
    val result = _result.asStateFlow()

    init {
        getForm()
    }

    fun getForm() {
        viewModelScope.launch(Dispatchers.IO) {
            handleNetworkFlow(getFormUseCase.execute(params = null), onEach = { form ->
                _form.emit(UiState.Ready(form))
            }, catch = {
                _form.emit(UiState.getErrorUiState(it))
            }).collect()
        }
    }

    fun post(postFormDto: PostFormDto) {
        viewModelScope.launch(Dispatchers.IO) {
            handleNetworkFlow(postFormUseCase.execute(params = postFormDto), onEach = {
                _result.emit(UiState.Ready(it.result ?: ""))
                Log.e(TAG, "post: $it")
            }, catch = {
                _result.emit(UiState.getErrorUiState(it))
                Log.e(TAG, "post: $it")
            }).collect()
        }
    }
}