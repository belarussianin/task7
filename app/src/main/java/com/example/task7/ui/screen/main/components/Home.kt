package com.example.task7.ui.screen.main.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.example.task7.common.UiState
import com.example.task7.common.UiState.Companion.Handle
import com.example.task7.data.remote.dto.GetFormDto
import com.example.task7.data.remote.dto.PostFormDto

@Composable
fun Home(
    form: UiState<GetFormDto, String>,
    result: UiState<String, String>,
    innerPadding: Dp = 8.dp,
    onSubmit: (PostFormDto) -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(innerPadding),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        form.Handle(
            onLoading = {
                CircularProgressIndicator()
            },
            onReady = { formDto ->
                Form(getFormDto = formDto) {
                    onSubmit(it)
                }
            },
            onError = {
                Text(text = it)
            }
        )
        result.Handle(
            onReady = {
                Text(text = it)
            },
            onError = {
                Text(text = it)
            }
        )
    }
}