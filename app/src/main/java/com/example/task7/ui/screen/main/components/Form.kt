package com.example.task7.ui.screen.main.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import coil.compose.AsyncImage
import com.example.task7.data.remote.dto.GetFormDto
import com.example.task7.data.remote.dto.PostField
import com.example.task7.data.remote.dto.PostFormDto
import kotlinx.coroutines.delay

@Composable
fun Form(
    getFormDto: GetFormDto,
    onSubmit: (PostFormDto) -> Unit
) {
    val save = remember { mutableStateOf(false) }
    getFormDto.apply {
        image?.let {
            AsyncImage(
                model = it,
                contentDescription = null,
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentHeight()
            )
        }
        title?.let {
            Text(text = it)
        }
        fields?.let {
            val result = mutableListOf<PostField>()
            LazyColumn(
                verticalArrangement = Arrangement.SpaceBetween,
                horizontalAlignment = Alignment.Start
            ) {
                itemsIndexed(it) { index, field ->
                    FormFieldItem(field = field, save = save.value, onSave = {
                        result.add(index, it)
                    })
                }
            }
            if (save.value) {
                LaunchedEffect(Unit) {
                    // TODO Костыль, на последнем дыхании, на неработающую голову...
                    delay(1000)
                    onSubmit(PostFormDto(result.associateBy({ it.name }, { it.value })))
                    save.value = false
                }
            }
        }
        Button(onClick = {
            save.value = true
        }) {
            Text(text = "Submit")
        }
    }
}