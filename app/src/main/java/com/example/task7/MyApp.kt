package com.example.task7

import android.app.Application
import com.example.task7.di.component.AppComponent
import com.example.task7.di.component.DaggerAppComponent

class MyApp : Application() {

    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.create()
    }
}