package com.example.task7.data.remote

import com.example.task7.data.remote.dto.GetFormDto
import com.example.task7.data.remote.dto.PostFormDto
import com.example.task7.data.remote.dto.Result
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface TestAPI {
    @GET(GET_META)
    suspend fun get(): GetFormDto

    @POST(POST_DATA)
    suspend fun post(@Body postFormDto: PostFormDto): Result
}