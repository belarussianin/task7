package com.example.task7.data.remote.dto

data class GetFormDto(
    val fields: List<Field>?,
    val image: String?,
    val title: String?
)