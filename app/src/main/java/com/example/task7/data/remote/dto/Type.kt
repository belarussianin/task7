package com.example.task7.data.remote.dto

sealed class Type(
    val string: String
) {
    object Text : Type("TEXT")
    object Numeric : Type("NUMERIC")
    object List : Type("LIST")
}
