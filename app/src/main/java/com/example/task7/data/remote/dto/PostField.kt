package com.example.task7.data.remote.dto

data class PostField(
    val name: String,
    val value: String
)
