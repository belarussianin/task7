package com.example.task7.data.remote.dto

data class Field(
    val name: String?,
    val title: String?,
    val type: String?,
    val values: Values?
)