package com.example.task7.data.remote.dto

data class PostFormDto(
    val form: Map<String, String>
)
