package com.example.task7.data.remote.dto

data class Values(
    val none: String?,
    val v1: String?,
    val v2: String?,
    val v3: String?
)