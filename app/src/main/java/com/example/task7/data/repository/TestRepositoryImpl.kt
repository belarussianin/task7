package com.example.task7.data.repository

import com.example.task7.data.remote.TestAPI
import com.example.task7.data.remote.dto.PostFormDto
import com.example.task7.domain.repository.TestRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TestRepositoryImpl @Inject constructor(
    private val testAPI: TestAPI
) : TestRepository {
    override suspend fun get() = testAPI.get()

    override suspend fun post(postFormDto: PostFormDto) = testAPI.post(postFormDto)
}