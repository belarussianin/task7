package com.example.task7.common

import android.content.Context
import com.example.task7.MyApp
import com.example.task7.di.component.AppComponent

val Context.appComponent: AppComponent
    get() = when (this) {
        is MyApp -> appComponent
        else -> this.applicationContext.appComponent
    }