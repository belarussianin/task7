package com.example.task7.common

import androidx.compose.runtime.Composable

sealed class UiState<out T, out R> {
    class Ready<out T>(val data: T) : UiState<T, Nothing>()
    class Error<out R>(val error: R) : UiState<Nothing, R>()
    object Loading : UiState<Nothing, Nothing>()

    companion object {

        @Composable
        inline fun <T, R> UiState<T, R>.Handle(
            onReady: @Composable (T) -> Unit = {},
            onError: @Composable (R) -> Unit = {},
            onLoading: @Composable () -> Unit = {}
        ): Unit {
            when (this) {
                is Ready -> onReady(data)
                is Error -> onError(error)
                is Loading -> onLoading()
            }
        }

        fun getErrorUiState(throwable: Throwable) = Error(
            error = throwable.localizedMessage ?: throwable.message ?: "something went wrong"
        )
    }
}